import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <span className="ml-1">CEVT &copy; 2020 Univ Evry</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">{new Date().toString()}</span>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
