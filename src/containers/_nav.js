export default [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: 'cil-speedometer',
    badge: {
      color: 'info'
    }
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Calendrier',
    to: '/calendar',
    icon: 'cil-calendar'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Outils']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Evenements',
    to: '/events',
    icon: 'cil-group'
  }
  /*
  {
    _tag: 'CSidebarNavItem',
    name: 'Statistiques',
    to: '/charts',
    icon: 'cil-chart-pie'
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Groupes',
    route: '/groups',
    icon: 'cil-group',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Gestion des groupes',
        to: '/groups'
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Enseignants',
    route: '/buttons',
    icon: 'cil-clipboard',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Gestion des enseignants',
        to: '/enseignants',
      }
    ],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Exports',
    route: '/icons',
    icon: 'cil-star',
    badge: {
      color: 'secondary',
      text: 'En cours',
    },
    addLinkClass: 'c-disabled',
    'disabled': true,
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Informations',
    route: '/notifications',
    icon: 'cil-bell',
    badge: {
      color: 'secondary',
      text: 'En cours',
    },
    addLinkClass: 'c-disabled',
    'disabled': true,
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Profile',
    to: '/widgets',
    icon: 'cil-user',
    badge: {
      color: 'secondary',
      text: 'En cours',
    },
    addLinkClass: 'c-disabled',
    'disabled': true,
  },
  {
    _tag: 'CSidebarNavDivider'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Extras'],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Mentions légales',
    to: '/licence',
    icon: 'cil-chart-pie'
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Licence',
    icon: 'cil-ban',
    badge: {
      color: 'secondary',
      text: 'En cours',
    },
    addLinkClass: 'c-disabled',
    'disabled': true
  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  }*/
]

