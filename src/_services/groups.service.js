export {
    getGroups,
    addGroup,
    deleteGroup
};



function getGroups (){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/group-names/all';
    var response = fetch(apiUrl)
        .then(response => response.json())
    return response
}

function addGroup (group){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/group-names/add';
    var response = fetch(apiUrl, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(group)
    })
    return response
}

function deleteGroup (name){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/group-names/delete';
    var response = fetch(apiUrl+"?name="+name, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "DELETE",
    })
    return response
}