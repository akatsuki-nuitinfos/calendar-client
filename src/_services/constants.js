export const CM = "CM";
export const TP = "TP";
export const TD = "TD";



export const DashboardFields = [
    { key: 'calName', label: 'Enseignant',  _style: { width: '40%'} },
    { key: 'cm', label: 'CM Effectué',  _style: { width: '20%'} },
    { key: 'td', label: 'TD Effectué',  _style: { width: '20%'} },
    { key: 'tp',  label: 'TP Effectué', _style: { width: '20%'} }
  ];

  export const DataTableFields = [
    { key: 'firstName', label: 'Prénom',  _style: { width: '25%'} },
    { key: 'lastName', label: 'Nom',  _style: { width: '25%'} },
    { key: 'email', label: 'Adresse mail',  _style: { width: '50%'} },
  ];

  export const GroupFields = [
    { key: 'name', label: 'Nom du groupe',  _style: { width: '20%'} },
    { key: 'link', label: 'URL du calendrier ICS',  _style: { width: '80%'} },
    {
      key: 'actions',
      label: 'Actions',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ];

  export const TeachersFields = [
    { key: 'lastName', label: 'Nom',  _style: { width: '50%'} },
    { key: 'firstName', label: 'Prénom',  _style: { width: '50%'} },
    {
      key: 'actions',
      label: 'Actions',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ];

  export const EventDetails = [
    { key: 'summary', label: 'Intitulé',  _style: { width: '10%'} },
    { key: 'dtStart', label: 'Date de début',  _style: { width: '10%'} },
    { key: 'dtEnd', label: 'Date de fin',  _style: { width: '10%'} },
    { key: 'location', label: 'Salle',  _style: { width: '10%'} },
    { key: 'matiere', label: 'Matière',  _style: { width: '10%'} },
    { key: 'groupe', label: 'Groupe',  _style: { width: '10%'} },

  ];

//// LANGUAGES SUPPORTED 
export const FR = "FR";
export const EN = "EN";


export const avgEvolutionTitle = 
new Map([
    [FR, "Evolution de la moyenne d'heures d'enseignements par mois"],
    [EN, "Evolution of the average teaching hours per month"],
  ]);