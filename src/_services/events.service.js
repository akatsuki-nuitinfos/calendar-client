export {
    addEvent,
    getEvents
};


function addEvent (group){
    var user = localStorage.getItem('user')
    group.calName= user.firstName.toUpperCase()+' '+user.lastName.toUpperCase()
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/calendar/teachers-events/'+user.lastName+'/'+user.firstName;
    var response = fetch(apiUrl, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(group)
    })
    return response
}

function getEvents (){
    var user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/calendar/teachers-events/'+user.lastName+'/'+user.firstName;
    var response = fetch(apiUrl)
        .then(response => response.json())
    return response
}