export {
    getTeachersList,
    getTeacherDetails,
    getAverages,
    getAllTeacherDetails,
    getMonthlyAverages,
    addTeacher,
    deleteTeacher
};


function getTeachersList (){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/teachers/all';
    var response = fetch(apiUrl)
        .then(response => response.json())
    return response
}

function getTeacherDetails (firstName, lastName) {
    var response = fetch(process.env.REACT_APP_API_URL+'/api/v1/stats/teacher/details/'+firstName+'/'+lastName)
      .then(response => response.json());
    return response
}

function getAverages () {
    var response = fetch(process.env.REACT_APP_API_URL+'/api/v1/stats/annual-averages')
      .then(response => response.json());
    return response
}

function getAllTeacherDetails () {
    var details = []
    var response = getTeachersList()
        .then(async (data) => {
            await Promise.all(data.map((teacher)=> {
                return getTeacherDetails(teacher.firstName, teacher.lastName).then((data)=> {
                    details.push(data)
                })
            }))
            return details
        })
    return response
}

function getMonthlyAverages () {
    var response = fetch(process.env.REACT_APP_API_URL+'/api/v1/stats/monthly-total')
      .then(response => response.json());
    return response
}

function addTeacher(teacher) {
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/teachers/add';
    var response = fetch(apiUrl, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(teacher)
    })
    return response
}

function deleteTeacher (email){
    const apiUrl = process.env.REACT_APP_API_URL+'/api/v1/group-names/delete';
    var response = fetch(apiUrl+"?email="+email, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "DELETE",
    })
    return response
}