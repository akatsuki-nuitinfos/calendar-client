import React, { Component } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CWidgetProgress,
  CDataTable,
} from '@coreui/react'

import MainChartExample from '../charts/MainChartExample.js'

import * as Constants from '../../_services/constants';

import {
  getAverages,
  getAllTeacherDetails,
  getMonthlyAverages,
  getTeachersList
} from '../../_services/teachers.service';

class Dashboard extends Component {

   

  constructor(props) {
    super(props)
    this.supportedLanguage = Constants.FR;
    this.isLoading = true;
    this.state= {
      averages: {},
      totals: {},
      numberTeachers: 0,
      teachersList: {},
      monthlyAverages: [],
      dataTableData : [],
    }
  }

  async componentDidMount() {
    this.initData();
  }

  initData() {
    getTeachersList().then((teachers) => {
      console.log(teachers)
          this.isLoading  = false;
          this.setState({
             dataTableData : teachers,
            })
    });
  }

  render() {
    return (
      <>
      { this.isLoading === false ? <>
        <CCard>
        <CCardHeader>
                Liste des enseignants
              </CCardHeader>
          <CCardBody  >

        <CDataTable

          items={this.state.dataTableData}
          fields={Constants.DataTableFields}
          columnFilter
          tableFilter
          itemsPerPageSelect
          itemsPerPage={5}
          hover
          sorter
          pagination

      />
      </CCardBody>
       </CCard>


      </> : <div className="text-center">
              <img src="https://i.stack.imgur.com/hzk6C.gif" alt="Loading" style={{ position: 'absolute', top: '30%', left: '45%', width: '300px', height: '200px' }}/>       
            </div>
      }
      </>
    )
  }
}

export default Dashboard
