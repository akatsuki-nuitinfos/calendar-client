import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CFormGroup,
  CInput,
  CLabel,
  CRow,
  CDataTable,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import * as Constants from '../../_services/constants';

import {
  getTeachersList,
  addTeacher,
  deleteTeacher
} from '../../_services/teachers.service';

const Teachers = () => {

  const[teachers, setTeachers] = useState([])
  const[form, setForm] = useState({
      cm : '1', 
      td:'1',
      tp: '1',
      userId: '',
      lastName: "",
      firstName: "",
      email: "",
      status: "",
      quotite: "",
      department: "",
      composante: "",
      service: "",
      pourcentage: ""
    })

  useEffect(()=> {
    getTeachersList().then(data => {
      setTeachers(data)
    })
  }, [])

  const handleSubmit = e => {
    //console.log(form)
    addTeacher(form).then((response) => {
      if(response.status === 200 && validate(form.email)) {
        alert("L'enseignant "+form.lastName+' '+form.firstName+" a été enregistré avec succès.")
        handleClear()
        getTeachersList().then(data => {
          setTeachers(data)
        })
      } else {
        alert("Erreur")
      }
    })
  }

  const handleDelete = item => {
    //console.log(item)
    if(window.confirm("Voulez-vous supprimer l'enseignant "+item.name+" ?")) {
      deleteTeacher(item.name).then((response) => {
        if(response.status === 200) {
          alert("Le groupe "+form.groupName+" a été supprimé.")
        } else {
          alert("Erreur")
        }
      })
      getTeachersList().then(data => {
        setTeachers(data)
      })
    }
  }

  const changeHandler = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleClear = e => {
    setForm({
        cm : '', 
        td:'',
        tp: '',
        userId: '',
        lastName: "",
        firstName: "",
        email: "",
        status: "",
        quotite: "",
        department: "",
        composante: "",
        service: "",
        pourcentage: ""
      })
  }

  const modifyTable = item => {
    setForm({
      groupName: item.name,
      groupURL :item.link
    })
  }

  const validate = (email) => {
    const expression = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]/;
    return expression.test(String(email).toLowerCase())
}

  return (
    <>
     <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
              Enseignants
            </CCardHeader>
            <CCardBody>
            <CDataTable
              items={teachers}
              fields={Constants.TeachersFields}
              columnFilter
              tableFilter
              itemsPerPageSelect
              itemsPerPage={5}
              hover
              sorter
              pagination
              scopedSlots={{
                'actions': 
                  (item) => {
                    return(
                      <td className="py-2">
                        <CDropdown className="mt-2">
                          <CDropdownToggle caret color="info">
                            Modifier
                          </CDropdownToggle>
                          <CDropdownMenu>
                            <CDropdownItem onClick={e => modifyTable(item)}>Modifier</CDropdownItem>
                            <CDropdownItem onClick={e => handleDelete(item)}>Supprimer</CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </td>
                    )
                  }
              }}
            />
            </CCardBody>
            
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
              Enseignants
            </CCardHeader>
            <CCardBody>
              <CFormGroup>
                  <CRow>
                      <CCol xs="4">
                        <CLabel htmlFor="inputIsValid">Coeff CM</CLabel>
                        <CInput id="cm" name="cm" onChange={changeHandler} placeholder="Coeff des séances CM. Exemple : 1.5" value={form.cm}/>
                      </CCol>
                      <CCol xs="4">
                        <CLabel htmlFor="inputIsValid">Coeff TD</CLabel>
                        <CInput id="td" name="td" onChange={changeHandler} placeholder="Coeff des séances TD. Exemple : 1.5" value={form.td}/>
                      </CCol>
                      <CCol xs="4">
                        <CLabel htmlFor="inputIsValid">Coeff TP</CLabel>
                        <CInput id="tp" name="tp" onChange={changeHandler} placeholder="Coeff des séances TP. Exemple : 1.5" value={form.tp}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="6">
                        <CLabel htmlFor="inputIsValid">Prénom</CLabel>
                        <CInput id="firstName" name="firstName" onChange={changeHandler} placeholder="Veuillez choisir un identifiant unique." value={form.firstName}/>
                      </CCol>
                      <CCol xs="6">
                        <CLabel htmlFor="inputIsValid">Nom</CLabel>
                        <CInput id="lastName" name="lastName" onChange={changeHandler} placeholder="Veuillez choisir un identifiant unique." value={form.lastName}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Email</CLabel>
                        <CInput id="email" name="email" onChange={changeHandler} placeholder="Veuillez insérer votre adresse mail." value={form.email}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Statut</CLabel>
                        <CInput id="status" name="status" onChange={changeHandler} placeholder="Veuillez insérer votre statut." value={form.status}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Quotite</CLabel>
                        <CInput id="quotite" name="quotite" onChange={changeHandler} placeholder="Veuillez insérer votre quotite." value={form.quotite}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Département</CLabel>
                        <CInput id="department" name="department" onChange={changeHandler} placeholder="Veuillez insérer votre département." value={form.department}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Composant</CLabel>
                        <CInput id="composante" name="composante" onChange={changeHandler} placeholder="Veuillez insérer votre composant." value={form.composante}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Service</CLabel>
                        <CInput id="service" name="service" onChange={changeHandler} placeholder="Veuillez insérer votre service." value={form.service}/>
                      </CCol>
                  </CRow>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Pourcentage</CLabel>
                        <CInput id="pourcentage" name="pourcentage" onChange={changeHandler} placeholder="Veuillez choisir un identifiant unique." value={form.pourcentage}/>
                      </CCol>
                  </CRow>
              </CFormGroup>
              <CButton type="submit" onClick={handleSubmit} size="sm" color="success" className="mx-1 float-right"><CIcon name="cil-save" /> Enregistrer</CButton>
              <CButton type="reset" onClick={handleClear} size="sm" color="danger" className="mx-1 float-right"><CIcon name="cil-ban" /> Effacer</CButton>
            </CCardBody>
             
          </CCard>
        </CCol>
      </CRow>

    </>
  )
}

export default Teachers
