import React from 'react'
import { CChartLine } from '@coreui/react-chartjs'
import { getStyle, hexToRgba } from '@coreui/utils/src'

const brandSuccess = getStyle('success') || '#4dbd74'
const brandInfo = getStyle('info') || '#20a8d8'
const brandDanger = getStyle('info') || '#f86c6b'

const MainChartExample = attributes => {
  //console.log(attributes.dataset)

  var labels = []

  const Datasets = (()=>{
    const CM = []
    const TD = []
    const TP = []
    attributes.dataset.forEach(element => {
      CM.push(element.avgCM)
      TD.push(element.avgTD)
      TP.push(element.avgTP)
      labels.push(element.date)
    });
    return [
      {
        label: 'Moyenne d\'heures des séances CM',
        backgroundColor: hexToRgba(brandInfo, 10),
        borderColor: brandInfo,
        pointHoverBackgroundColor: brandInfo,
        borderWidth: 2,
        data: CM
      },
      {
        label: 'Moyenne d\'heures des séances TD',
        backgroundColor: hexToRgba(brandSuccess, 10),
        borderColor: brandSuccess,
        pointHoverBackgroundColor: brandSuccess,
        borderWidth: 2,
        data: TD
      },
      {
        label: 'Moyenne d\'heures des séances TP',
        backgroundColor: hexToRgba(brandDanger, 10),
        borderColor: brandDanger,
        pointHoverBackgroundColor: brandDanger,
        borderWidth: 2,
        data: TP
      }
    ]
  })()

  const defaultOptions = (()=>{
    return {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            gridLines: {
              drawOnChartArea: false
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
            },
            gridLines: {
              display: true
            }
          }]
        },
        elements: {
          point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3
          }
        }
      }
    }
  )()

  // render
  return (
    <CChartLine
      {...attributes}
      datasets={Datasets}
      options={defaultOptions}
      labels={labels}
    />
  )
}


export default MainChartExample
