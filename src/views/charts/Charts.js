import React from 'react'
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCardHeader
} from '@coreui/react'
import {
  CChartBar,
  CChartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs'

import {
  CFormGroup,
  CCol,
  CSelect
} from '@coreui/react'

import {
  getTeachersList,
  getTeacherDetails,
  getMonthlyAverages,
  getAverages
} from '../../_services/teachers.service';

import theme from './StylesCharts.scss'

class Charts extends React.Component {

  constructor(props) {
    super(props);
    this.state= {
      teachersDetails :{},
      monthlyTotal: {},
      annualAverages : {},
      teachersList: []

    }
  }

  componentDidMount() {
    getTeachersList().then((data) => {
      this.setState({ teachersList:  data })
    });
  }

  // Promise.all(promise, promise2 ...)
  selectTeacher = async (event) => {
    var selected = event.target.value.split(' ');
    var teacher = {firstName: selected[0], lastName: selected[1]}
    const promises = []
    promises.push(getTeacherDetails(teacher.firstName, teacher.lastName))
    promises.push(getMonthlyAverages())
    promises.push(getAverages())
    Promise.all(promises).then((result) => {

      //this.setMonthlyData(result[0].Stats.infos, result[1].map)
      
      this.setState({ 
        teachersDetails: result[0],
        monthlyTotal : result[1],
        annualAverages : result[2]
       })
    })
  }

  setMonthlyData = (monthlyData, monthlyAverages) => {

    var mapData = Object.entries(monthlyData)
    var mapAverages= Object.entries(monthlyAverages)

    var data = []
    var averages = []
    var check = false
    for(const [key,value] of mapAverages) {
      check = false
      //console.log(mapData)
      mapData.forEach(element => {
        //console.log(element[0])
        if(key === element[0]) {
          check = true
          data.push(element[1].cmTotal + element[1].tdTotal + element[1].tpTotal)
        }
      });
      if(!check) {
        data.push(0)
      }
      averages.push((value.cmTotal + value.tdTotal + value.tpTotal)/this.state.monthlyTotal.dataProviders.length)
    }
    this.barChartData(this.state.teachersDetails)
    return {data1 : data, data2: averages}
  }

  barChartData = (teachersDetails) => {
    var cm = []
    var td = []
    var tp = []

    var labels = []

    for(const [key,value] of Object.entries(teachersDetails.Stats.infos)) {
      labels.push(key)
      cm.push(value.cmTotal)
      td.push(value.tdTotal)
      tp.push(value.tpTotal)
    }

    return {labels : labels, cm: cm, td: td, tp: tp}

  }


  render () {
    return (
      <div theme={theme}>
      <CFormGroup row>
      <CCol xs="8" md="6">
        
        <CSelect custom name="select" id="select" onChange={this.selectTeacher}>
          {this.state.teachersList.map((item, index) =>
            <option key={index} >{item.firstName+' '+item.lastName}</option>
          )}
        </CSelect>
      </CCol>
    </CFormGroup>

    {Object.keys(this.state.teachersDetails).length !== 0 && 
    <CCardGroup columns className = "cols-2" >
    <CCard>
        <CCardHeader>
          Evolution actuelle de la charge de travail ( Réaliser / Total)
        </CCardHeader>
        <CCardBody>
          <CChartPolarArea
            type="polarArea"
            datasets={[
              {
                label: 'Total',
                backgroundColor: 'rgba(179,181,198,0.2)',
                pointBackgroundColor: 'rgba(179,181,198,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: 'rgba(179,181,198,1)',
                pointHoverBorderColor: 'rgba(179,181,198,1)',
                data: Object.values(this.state.teachersDetails.Total)
              },
              {
                label: 'Done',
                backgroundColor: 'rgba(255,99,132,0.2)',
                pointBackgroundColor: 'rgba(255,99,132,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: 'rgba(255,99,132,1)',
                pointHoverBorderColor: 'rgba(255,99,132,1)',
                data: Object.values(this.state.teachersDetails.Done)
              }
            ]}
            options={{
              aspectRatio: 1.5,
              tooltips: {
                enabled: true
              }
            }}
            labels={[
              'CM', 'TD', 'TP'
            ]}
          />
        </CCardBody>
      </CCard>
   



      <CCard>
        <CCardHeader>
          Dispersion des types d'enseignement par mois  (Total)
        
        </CCardHeader>
        <CCardBody>
          <CChartBar
            type="bar"
            datasets={[
              {
                label: 'CM',
                backgroundColor: '#34495e',
                data: this.barChartData(this.state.teachersDetails).cm
              },
              {
                label: 'TD',
                backgroundColor: '#00cec9',
                data: this.barChartData(this.state.teachersDetails).td
              },
              {
                label: 'TP',
                backgroundColor: '#3498db',
                data: this.barChartData(this.state.teachersDetails).tp
              }
            ]}
            labels={this.barChartData(this.state.teachersDetails).labels}
            options={{
              aspectRatio: 1.5,
              tooltips: {
                enabled: true
              }
            }}
          />
        </CCardBody>
      </CCard>



      <CCard>
        <CCardHeader>
          Travail Réaliser
        </CCardHeader>
        <CCardBody>
          <CChartDoughnut
            type="doughnut"
            datasets={[
              {
                backgroundColor: [
                  '#34495e',
                  '#00cec9',
                  '#3498db'
                ],
                data: [this.state.teachersDetails.Done.cm, this.state.teachersDetails.Done.td, this.state.teachersDetails.Done.tp]
              }
            ]}
            labels={['cm', 'td', 'tp']}
            options={{
              aspectRatio: 1.5,
              tooltips: {
                enabled: true
              }
            }}
          />
        </CCardBody>
      </CCard>



      <CCard>
        <CCardHeader v-c-tooltip="'I am <strong>tooltip</strong>'">
          Evolution de la charge de travail par rapport à la moyenne  (Total)
        </CCardHeader>
        <CCardBody>
          <CChartLine
            type="line"
            datasets={[
              {
                label: 'Moyenne générale',
                backgroundColor: 'rgba(179,181,198,0.2)',
                pointBackgroundColor: 'rgba(179,181,198,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: 'rgba(179,181,198,1)',
                pointHoverBorderColor: 'rgba(179,181,198,1)',
                data: this.setMonthlyData(this.state.teachersDetails.Stats.infos, this.state.monthlyTotal.map).data2
              },
              {
                label: 'Moyenne de l\'enseignant',
                backgroundColor: 'rgba(255,99,132,0.2)',
                pointBackgroundColor: 'rgba(255,99,132,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: 'rgba(255,99,132,1)',
                pointHoverBorderColor: 'rgba(255,99,132,1)',
                data: this.setMonthlyData(this.state.teachersDetails.Stats.infos, this.state.monthlyTotal.map).data1
              }
            ]}
            options={{
              aspectRatio: 1.5,
              tooltips: {
                enabled: true
              }
            }}
            labels={this.barChartData(this.state.teachersDetails).labels}
          />
        </CCardBody>
      </CCard>
      
      <CCard>
        <CCardHeader>
          Dispersion de la charge du professeur par rapport à la moyenne  (Total)
        </CCardHeader>
        <CCardBody>
          <CChartRadar
            type="radar"
            datasets={[
              {
                label: 'Moyenne de l\'enseignant',
                backgroundColor: 'rgba(179,181,198,0.2)',
                borderColor: 'rgba(179,181,198,1)',
                pointBackgroundColor: 'rgba(179,181,198,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(179,181,198,1)',
                tooltipLabelColor: 'rgba(179,181,198,1)',
                data: [this.state.teachersDetails.Total.cm, this.state.teachersDetails.Total.td, this.state.teachersDetails.Total.tp]
              },
              {
                label: 'Moyenne générale',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                pointBackgroundColor: 'rgba(255,99,132,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(255,99,132,1)',
                tooltipLabelColor: 'rgba(255,99,132,1)',
                data: [this.state.annualAverages.averages[0].average, this.state.annualAverages.averages[1].average, this.state.annualAverages.averages[2].average]
              }
            ]}
            options={{
              aspectRatio: 1.5,
              tooltips: {
                enabled: true
              }
            }}
            labels={[
              'cm', 'td', 'tp'
            ]}
          />
        </CCardBody>
      </CCard>
      
      <CCard>
        <CCardHeader>
          Total Prévu (Total)
        </CCardHeader>
        <CCardBody>
          <CChartPie
            type="pie"
            datasets={[
              {
                backgroundColor: [
                  '#34495e',
                  '#00cec9',
                  '#3498db'
                ],
                data: [this.state.teachersDetails.Total.cm, this.state.teachersDetails.Total.td, this.state.teachersDetails.Total.tp]
              }
            ]}
            labels={['CM', 'TD', 'TP']}
            options={{
              aspectRatio: 1.5,
              tooltips: {
                enabled: true
              }
            }}
          />
        </CCardBody>
      </CCard>
    </CCardGroup>}
    </div>
    );
  }
}

export default Charts
