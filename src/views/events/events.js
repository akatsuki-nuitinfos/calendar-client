import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CFormGroup,
  CInput,
  CLabel,
  CRow,
  CDataTable,
  
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import * as Constants from '../../_services/constants';
import DateTimePicker from 'react-datetime-picker';

import {
  getEvents,
  addEvent,
} from '../../_services/events.service';

const Events = () => {
    const [summary, summaryChange] = useState('');
    const [categories, categoriesChange] = useState('');
    const [location, locationChange] = useState('');
    const [matiere, matiereChange] = useState('');
    const [groupe, groupeChange] = useState('');
    const [dtStart, dtStartChange] = useState(new Date());
    const [dtEnd, dtEndChange] = useState(new Date());
    const[events, setEvents] = useState([])
    const [form, setForm] = useState({
        summary: '',
        categories: '',
        location: '',
        matiere: '',
        groupe: '',
        dtStart: new Date(),
        dtEnd: new Date()
    })

  useEffect(()=> {
    getEvents().then(data => {
      setEvents(data)
    })
  }, [])

  const handleSubmit = e => {
    //console.log(form)
    addEvent(form).then((response) => {
      if(response.status === 200) {
        alert("L'evenement "+form.lastName+' '+form.firstName+" a été enregistré avec succès.")
        handleClear()
        getEvents().then(data => {
          setEvents(data)
        })
      } else {
        alert("Erreur")
      }
    })
  }

  const changeHandler = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleClear = e => {
    setForm({
        summary: '',
        categories: '',
        location: '',
        matiere: '',
        groupe: '',
        dtStart: new Date(),
        dtEnd: new Date()
      })
  }

  return (
    <>
     <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
              Evenements
            </CCardHeader>
            <CCardBody>
            <CDataTable
              items={events}
              fields={Constants.EventDetails}
              columnFilter
              tableFilter
              itemsPerPageSelect
              itemsPerPage={5}
              hover
              sorter
              pagination
            />
            </CCardBody>
            
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
              Enseignants
            </CCardHeader>
            <CCardBody>
              <CFormGroup>
                  <CRow>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Intitulé</CLabel>
                        <CInput id="summary" name="summary" onChange={changeHandler} placeholder="Intitulé de l'event" value={form.summary}/>
                      </CCol>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Catégorie</CLabel>
                        <CInput id="categories" name="categories" onChange={changeHandler} placeholder="Catégorie de l'event" value={form.categories}/>
                      </CCol>
                      </CRow>
                      <CRow><CLabel htmlFor="nf-email">Période</CLabel></CRow>
                <CRow>
                    <CCol>
                      <DateTimePicker
                            onChange={dtStartChange}
                            value={form.dtStart}
                        />
                        </CCol>
                        <CCol>
                      <DateTimePicker
                            onChange={dtEndChange}
                            value={form.dtEnd}
                        />
                        </CCol>
                  </CRow>
                  <CRow>
                  <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Salle</CLabel>
                        <CInput id="location" name="location" onChange={changeHandler} placeholder="Salle de l'event" value={form.location}/>
                      </CCol>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Matière</CLabel>
                        <CInput id="matiere" name="matiere" onChange={changeHandler} placeholder="Matière" value={form.matiere}/>
                      </CCol>
                      <CCol xs="12">
                        <CLabel htmlFor="inputIsValid">Groupe</CLabel>
                        <CInput id="groupe" name="groupe" onChange={changeHandler} placeholder="Groupe concerné" value={form.groupe}/>
                      </CCol>
                  </CRow>
              </CFormGroup>
              <CButton type="submit" onClick={handleSubmit} size="sm" color="success" className="mx-1 float-right"><CIcon name="cil-save" /> Enregistrer</CButton>
              <CButton type="reset" onClick={handleClear} size="sm" color="danger" className="mx-1 float-right"><CIcon name="cil-ban" /> Effacer</CButton>
            </CCardBody>
             
          </CCard>
        </CCol>
      </CRow>

    </>
  )
}

export default Events
