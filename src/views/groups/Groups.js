import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CFormGroup,
  CValidFeedback,
  CInput,
  CLabel,
  CRow,
  CDataTable,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import * as Constants from '../../_services/constants';

import {
  getGroups,
  addGroup,
  deleteGroup
} from '../../_services/groups.service';

const Groups = () => {

  const[groups, setGroups] = useState([])
  const[form, setForm] = useState({groupName : '', groupURL:''})

  useEffect(()=> {
    getGroups().then(data => {
      setGroups(data)
    })
  }, [])

  const handleSubmit = e => {
    //console.log(form)
    addGroup([{
      name: form.groupName,
      link: form.groupURL
    }]).then((response) => {
      if(response.status === 200) {
        alert("Le groupe "+form.groupName+" a été enregistré avec succès.\nURL du calendrier du groupe : "+form.groupURL)
        getGroups().then(data => {
          setGroups(data)
        })
      } else {
        alert("Erreur")
      }
    })
  }

  const handleDelete = item => {
    //console.log(item)
    if(window.confirm("Voulez-vous supprimer le groupe "+item.name+" ?")) {
      deleteGroup(item.name).then((response) => {
        if(response.status === 200) {
          alert("Le groupe "+form.groupName+" a été supprimé.")
        } else {
          alert("Erreur")
        }
      })
      getGroups().then(data => {
        setGroups(data)
      })
    }
  }

  const changeHandler = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleClear = e => {
    setForm({
      groupName:'',
      groupURL:''
    })
  }

  const modifyTable = item => {
    setForm({
      groupName: item.name,
      groupURL :item.link
    })
  }

  return (
    <>
    <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
              Groupes
            </CCardHeader>
            <CCardBody>
            <CDataTable
              items={groups}
              fields={Constants.GroupFields}
              columnFilter
              tableFilter
              itemsPerPageSelect
              itemsPerPage={5}
              hover
              sorter
              pagination
              scopedSlots={{
                'actions': 
                  (item) => {
                    return(
                      <td className="py-2">
                        <CDropdown className="mt-2">
                          <CDropdownToggle caret color="info">
                            Modifier
                          </CDropdownToggle>
                          <CDropdownMenu>
                            <CDropdownItem onClick={e => modifyTable(item)}>Modifier</CDropdownItem>
                            <CDropdownItem onClick={e => handleDelete(item)}>Supprimer</CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </td>
                    )
                  }
              }}
            />
            </CCardBody>
           
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardHeader>
              Groupes
            </CCardHeader>
            <CCardBody>
              <CFormGroup>
                <CLabel htmlFor="inputIsValid">Identifiant</CLabel>
                <CInput id="groupName" name="groupName" onChange={changeHandler} placeholder="Veuillez insérer l'identifiant du groupe. Exemple : M2MIAA" value={form.groupName}/>
                <CValidFeedback>Cool! Input is valid</CValidFeedback>
              </CFormGroup>
              <CFormGroup>
                <CLabel htmlFor="inputIsValid">URL du calendrier</CLabel>
                <CInput id="groupURL" name="groupURL" onChange={changeHandler} placeholder="Veuillez insérer le lien du fichier ICS de l'EDT. Exemple : https://edt.univ-evry.fr/icsetudiant/m2miaa.ics" value={form.groupURL}/>
                <CValidFeedback>Cool! Input is valid</CValidFeedback>
              </CFormGroup>
              <CButton type="submit" onClick={handleSubmit} size="sm" color="success" className="mx-1 float-right"><CIcon name="cil-save" /> Enregistrer</CButton>
              <CButton type="reset" onClick={handleClear} size="sm" color="danger" className="mx-1 float-right"><CIcon name="cil-ban" /> Effacer</CButton>
            </CCardBody>
          
          </CCard>
        </CCol>
      </CRow>

    </>
  )
}

export default Groups
