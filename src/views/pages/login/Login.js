import React from 'react'
import {
  CButton,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CNavbar,
  CCollapse,
  CNavbarNav,
  CNavbarBrand,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { connect } from 'react-redux';
import { userActions } from '../../../_actions/user.actions';
import Calendar from 'cevt-calendar'
import '../../../views/calendar/calendar.style.scss'

class Login extends React.Component {

  constructor(props) {
    super(props)
    this.props.logout();
    this.state = {
      username: '',
      password: ''
    }
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit = e => {
    if(this.state.username && this.state.password) {
      const { username, password } = this.state;
      this.props.login(username, password);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.username !== nextState.username || this.state.password !== nextState.password) {
      return false;
    } else {
      return true;
    }
  }

  render() {
    return (
      <div>
        <CNavbar expandable="sm" color="info" >
        <CNavbarBrand>
          CEVT Dashboard
        </CNavbarBrand>
        <CCollapse navbar>
          <CNavbarNav className="ml-auto">
            <CForm inline>
            <CInputGroup className="mr-3">
              <CInputGroupPrepend>
                  <CInputGroupText>
                    <CIcon name="cil-user" />
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput type="text" name="username" onChange={this.handleChange} placeholder="Username" autoComplete="username" />
              </CInputGroup>
              <CInputGroup className="mr-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    <CIcon name="cil-lock-locked" />
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput type="password" name="password" onChange={this.handleChange}  placeholder="Password" autoComplete="current-password" />
              </CInputGroup>
              <CButton onClick={this.handleSubmit} color="primary" className="px-4">Se connecter</CButton>
            </CForm>
          </CNavbarNav>
        </CCollapse>
      </CNavbar>
      <br></br>
      
          <Calendar className="mt-2"  />
      
      
      </div>
    )
  }
}

function mapState(state) {
    const { loggingIn } = state.authentication;
    return { loggingIn };
}

const actionCreators = {
    login: userActions.login,
    logout: userActions.logout
};

export default connect(mapState, actionCreators)(Login);