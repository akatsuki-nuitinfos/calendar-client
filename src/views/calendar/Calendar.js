import React from 'react'
import Calendrier from 'cevt-calendar'
import {CButton} from '@coreui/react'
//<link href="https://cdn.syncfusion.com/ej2/material.css" rel="stylesheet" type="text/css">
import './calendar.style.scss'

const Calendar = () => {
  return (
    <>
    <CButton className="float-right success" color="success">Créer un événement</CButton>
      <Calendrier isTeacher={true} />
      </>
  )
}

export default Calendar
